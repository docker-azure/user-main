import React from "react";
import CommentItem from "./CommentItem";

function CommentList(props) {
  const { handleSetUpdate, comments } = props;

  return (
    <>
      {comments.map((comment) => (
        <CommentItem
          key={comment.id}
          comment={comment}
          handleSetUpdate={handleSetUpdate}
        />
      ))}
    </>
  );
}

export default CommentList;
