import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Modal, Rate, Popconfirm } from "antd";
import useToggle from "~/hooks/useToggle";
import toast from "~/helpers/toast";
import config from "~/constants";
import commentApi from "~/apis/commentApi";
import CommentInput from "./CommentInput";

function Comment(props) {
  const { handleSetUpdate } = props;
  const {
    star,
    comment,
    status,
    time,
    user,
    product_id: productId,
  } = props.comment;

  const currUser = useSelector((state) => state.userReducer.user);
  const [openModal, toggleModal] = useToggle(false);
  const [openPop, togglePop] = useToggle(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const onDeleteComment = () => {
    setConfirmLoading(true);
    const values = { product_id: productId };

    commentApi
      .deleteUserComment(values)
      .then((res) => {
        togglePop();
        setConfirmLoading(false);

        if (res.status === config.response.SUCCESS) {
          handleSetUpdate();
          return toast.success("Thành công", "Xóa bình luận thành công");
        }
        return toast.success("Thất bại", "Xóa bình luận thất bại");
      })
      .catch((err) => {
        togglePop();
        setConfirmLoading(false);
        return toast.success("Thất bại", "Xóa bình luận thất bại");
      });
  };

  return (
    <>
      <div className="comment-box">
        <div className="comment-avatar">
          <img src={config.api.AVATAR_IMAGE + user.image} alt={user.name} />
        </div>
        <div className="comment-body">
          <div className="comment-content">
            <h3 className="comment-user">{user.name}</h3>
            <div className="comment-text">{comment}</div>
            <div className="comment-rating-time">
              <div className="comment-rating">
                <Rate
                  disabled
                  character={<i className="fas fa-star" />}
                  value={star}
                />
              </div>
              <p className="comment-time">{time}</p>
            </div>
          </div>
          {user.id === currUser.id && (
            <div className="comment-feature">
              {status === "0" ? (
                <i className="fas fa-spinner" />
              ) : (
                <i className="fas fa-check-circle" />
              )}
              <i className="fas fa-edit" onClick={toggleModal} />
              <Popconfirm
                title="Bạn có thật sự muốn xóa ?"
                visible={openPop}
                onConfirm={onDeleteComment}
                okButtonProps={{ loading: confirmLoading }}
                onCancel={togglePop}
                okText="Có chứ"
                cancelText="Không nha"
              >
                <i className="fas fa-trash" onClick={togglePop} />
              </Popconfirm>
            </div>
          )}
        </div>
      </div>
      <Modal
        visible={openModal}
        onOk={toggleModal}
        onCancel={toggleModal}
        footer={null}
      >
        <CommentInput
          title="Chỉnh sửa bình luận"
          buttonText="Cập nhập"
          productId={productId}
          comment={props.comment}
          cancelModal={toggleModal}
          handleSetUpdate={handleSetUpdate}
        />
      </Modal>
    </>
  );
}

export default Comment;
