import React from "react";
import { useTranslation } from "react-i18next";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import OrderItem from "./OrderItem";
import { Empty } from "antd";

function OrderList(props) {
  const { orders } = props;
  const { t } = useTranslation();

  return (
    <>
      {orders && orders.length > 0 && (
        <table className="table">
          <thead>
            <tr>
              <th>#</th>
              <th>{t("order.code_order")}</th>
              <th>{t("order.customer_name")}</th>
              <th>{t("common.status")}</th>
              <th>{t("common.total_money")}</th>
              <th>{t("common.time")}</th>
              <th>{t("common.action")}</th>
            </tr>
          </thead>
          <tbody>
            {orders.map((order, index) => (
              <OrderItem key={order.id} order={order} index={index + 1} />
            ))}
          </tbody>
        </table>
      )}
      {orders && orders.length <= 0 && (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description={<span>Không có đơn hàng nào</span>}
        ></Empty>
      )}
      {!orders && (
        <Skeleton
          count={5}
          height={"30px"}
          duration={1}
          style={{ margin: "6px 0" }}
        />
      )}
    </>
  );
}

export default OrderList;
