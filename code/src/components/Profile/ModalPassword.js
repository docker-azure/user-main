import React, { useState } from "react";
import { Modal, Button } from "antd";
import { useFormik } from "formik";
import userApi from "~/apis/userApi";
import config from "~/constants";
import { passwordValid } from "~/helpers/validate";
import toast from "~/helpers/toast";

function ModalPassword(props) {
  const { openPassword, togglePassword } = props;
  const [confirmLoading, setConfirmLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      old_password: "",
      new_password: "",
      pre_password: "",
    },
    validationSchema: passwordValid,
    onSubmit: (value) => {
      setConfirmLoading(true);
      userApi
        .updatePassword(value)
        .then((res) => {
          setConfirmLoading(false);
          if (res.status === config.response.SUCCESS) {
            togglePassword();
            toast.success("Thành công", "Thay đổi mật khẩu thành công");
          } else {
            toast.error("Thất bại", "Mật khẩu hiện tại không đúng");
            formik.setErrors({
              old_password: "Mật khẩu hiện tại không đúng",
            });
          }
        })
        .catch((err) => {
          setConfirmLoading(false);
          toast.error("Thất bại", "Vui lòng thử lại");
        });
    },
  });

  return (
    <Modal visible={openPassword} onCancel={togglePassword} footer={null}>
      <div className="form-section">
        <h3 className="user-info">Thay đổi mật khẩu</h3>
        <form onSubmit={formik.handleSubmit} className="form-info">
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-key" />
              <input
                type="passowrd"
                name="old_password"
                className="form-control"
                value={formik.values.old_password}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Mật khẩu cũ..."
              />
            </div>
            {formik.errors.old_password && formik.touched.old_password && (
              <p className="error">{formik.errors.old_password}</p>
            )}
          </div>
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-key" />
              <input
                type="passowrd"
                name="new_password"
                className="form-control"
                value={formik.values.new_password}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Mật khẩu mới..."
              />
            </div>
            {formik.errors.new_password && formik.touched.new_password && (
              <p className="error">{formik.errors.new_password}</p>
            )}
          </div>
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-key" />
              <input
                type="passowrd"
                name="pre_password"
                className="form-control"
                value={formik.values.pre_password}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Nhập lại mật khẩu mới..."
              />
            </div>
            {formik.errors.pre_password && formik.touched.pre_password && (
              <p className="error">{formik.errors.pre_password}</p>
            )}
          </div>

          <div className="order-button">
            <Button
              className="btn btn-order"
              loading={confirmLoading}
              htmlType="submit"
            >
              Cập nhập
            </Button>
          </div>
        </form>
      </div>
    </Modal>
  );
}

export default ModalPassword;
