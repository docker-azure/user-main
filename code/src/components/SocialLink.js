import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import userApi from "~/apis/userApi";
import config from "~/constants";
import WaveLoading from "./Loading/WaveLoading";

function SocialLink() {
  const [signinGoogle, setSigninGoogle] = useState(null);
  const [signinFacebook, setSigninFacebook] = useState(null);
  const [signinGithub, setSigninGithub] = useState(null);
  const [signinLinkedIn, setSigninLinkedIn] = useState(null);
  const { t } = useTranslation();

  useEffect(() => {
    userApi
      .signInSocial("google")
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          setSigninGoogle(res.data);
        }
      })
      .catch((err) => {});

    userApi
      .signInSocial("facebook")
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          setSigninFacebook(res.data);
        }
      })
      .catch((err) => {});
    userApi
      .signInSocial("github")
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          setSigninGithub(res.data);
        }
      })
      .catch((err) => {});
    userApi
      .signInSocial("linkedin")
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          setSigninLinkedIn(res.data);
        }
      })
      .catch((err) => {});
  }, []);

  return (
    <>
      <p className="social-text">{t("common.sign_in_social")}</p>

      <div className="social-media">
        {signinGoogle && (
          <a href={signinGoogle} className="social-media-link">
            <div className="social-media-item instagram">
              <i className="fab fa-google social-media-icon" />
              <span className="social-media-text">Google</span>
            </div>
          </a>
        )}

        {signinFacebook && (
          <a href={signinFacebook} className="social-media-link">
            <div className="social-media-item facebook">
              <i className="fab fa-facebook social-media-icon" />
              <span className="social-media-text">Facebook</span>
            </div>
          </a>
        )}
      </div>

      <div className="social-media">
        {signinGithub && (
          <a href={signinGithub} className="social-media-link">
            <div className="social-media-item instagram">
              <i className="fab fa-github social-media-icon" />
              <span className="social-media-text">Github</span>
            </div>
          </a>
        )}

        {signinLinkedIn && (
          <a href={signinLinkedIn} className="social-media-link">
            <div className="social-media-item facebook">
              <i className="fab fa-linkedin-in social-media-icon" />
              <span className="social-media-text">LinkedIn</span>
            </div>
          </a>
        )}
      </div>
      {!signinGoogle && (
        <div style={{ marginTop: "12px" }}>
          <WaveLoading />
        </div>
      )}
    </>
  );
}

export default SocialLink;
