import React from "react";
import { useTranslation } from "react-i18next";
import FacebookPage from "~/components/Facebook/FacebookPage";
import mwstoreLogo from "~/assets/img/logo/mwstore.png";

function Footer() {
  const { t } = useTranslation();

  return (
    <footer className="footer">
      <div id="fb-root"></div>
      <div className="gird-full">
        <div className="row">
          <div className="footer-content col-12 col-md-4 col-lg-3 col-xl-3">
            <div className="footer-logo">
              <img src={mwstoreLogo} alt="mwstore logo" />
            </div>
          </div>
          <div className="footer-content col-12 col-md-4 col-lg- col-xl-6">
            <div className="col-4">
              <p className="footer-info">{t("footer.rules")}</p>
              <p className="footer-info">{t("footer.service")}</p>
              <p className="footer-info">{t("footer.policy")}</p>
            </div>
            <div className="col-4">
              <p className="footer-info">{t("footer.evaluate")}</p>
              <p className="footer-info">{t("footer.contact")}</p>
              <p className="footer-info">{t("footer.about_me")}</p>
            </div>
            <div className="col-4">
              <p className="footer-info">{t("footer.rules")}</p>
              <p className="footer-info">{t("footer.service")}</p>
              <p className="footer-info">{t("footer.policy")}</p>
            </div>
          </div>
          <div className="footer-content col-12 col-md-4 col-lg-3 col-xl-3">
            <FacebookPage />
          </div>
        </div>
        <div className="row">
          <p className="footer-end-text">{t("footer.footer_title")}</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
