import React, { useState, useEffect, createContext } from "react";
import { Modal } from "antd";
import useToggle from "~/hooks/useToggle";
import Header from "./Header";
import Container from "./Container";

const player = new Audio();
player.setAttribute("preload", "metadata");

const userOptions = createContext({
  shuffle: false,
  repeat: false,
});

function MusicPlayer() {
  const [openMusic, toggleMusic] = useToggle(false);
  const [tracks, setTracks] = useState([]);

  // useEffect(() => {
  //   fetch(
  //     "https://api.apify.com/v2/key-value-stores/EJ3Ppyr2t73Ifit64/records/LATEST"
  //   )
  //     .then((res) => res.json())
  //     .then((res) => {
  //       const songs = res.songs.top100_VN[0].songs;
  //       const newSongs = songs.map((song) => {
  //         const { title, creator, bgImage, music, url } = song;

  //         return {
  //           name: title,
  //           artist: creator,
  //           cover: bgImage,
  //           source: music,
  //           url: url,
  //         };
  //       });

  //       setTracks(newSongs);
  //       player.src = newSongs[0].source;
  //     })
  //     .catch((err) => {});
  // }, []);

  return (
    <>
      <div className="btn-music" onClick={toggleMusic}>
        <i className="fas fa-compact-disc"></i>
      </div>
      <Modal
        bodyStyle={{ padding: "8px" }}
        visible={openMusic}
        onCancel={toggleMusic}
        closable={false}
        footer={null}
      >
        <div className="music">
          <Header />
          <Container
            player={player}
            tracks={tracks}
            userOptions={userOptions}
          />
        </div>
      </Modal>
    </>
  );
}

export default MusicPlayer;
