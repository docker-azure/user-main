import { ThreeDots, ChevronLeft } from "react-bootstrap-icons";

function Header() {
  return (
    <div className="music-header">
      <button className="music-icon">
        <ChevronLeft />
      </button>
      <h1 className="music-headerText">Phát nhạc</h1>
      <button className="music-icon">
        <ThreeDots />
      </button>
    </div>
  );
}

export default Header;
