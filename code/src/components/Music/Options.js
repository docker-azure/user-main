import React, { useState, useContext } from "react";
import {
  Shuffle,
  ArrowRepeat,
  BoxArrowUpRight,
  Download,
} from "react-bootstrap-icons";

function Options(props) {
  const { idx, tracks, userOptions } = props;
  let options = useContext(userOptions);
  let [shuffl, setShuffle] = useState(options.shuffle);
  let [repet, setRepeat] = useState(options.repeat);

  function shuffle() {
    options.shuffle = !options.shuffle;
    options.repeat = false;
    setShuffle(!shuffl);
    setRepeat(false);
  }

  function repeat() {
    options.repeat = !options.repeat;
    options.shuffle = false;
    setShuffle(false);
    setRepeat(!repet);
  }

  function downloadMusic() {
    window.open(tracks[idx].source, "_blank");
  }

  function openURL() {
    window.open(tracks[idx].url, "_blank");
  }

  return (
    <div className="music-options">
      {(shuffl && (
        <button
          onClick={shuffle}
          className="music-opt"
          style={{ color: "#147CC0" }}
        >
          <Shuffle />
        </button>
      )) || (
        <button onClick={shuffle} className="music-opt">
          <Shuffle />
        </button>
      )}
      <button className="music-opt" onClick={openURL}>
        <BoxArrowUpRight />
      </button>
      <button className="music-opt" onClick={downloadMusic}>
        <Download />
      </button>
      {(repet && (
        <button
          onClick={repeat}
          className="music-opt"
          style={{ color: "#147CC0" }}
        >
          <ArrowRepeat />
        </button>
      )) || (
        <button onClick={repeat} className="music-opt">
          <ArrowRepeat />
        </button>
      )}
    </div>
  );
}

export default Options;
