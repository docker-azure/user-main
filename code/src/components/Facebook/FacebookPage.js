import React from "react";
import useFacebookSDK from "~/hooks/useFacebookSDK";

function FacebookPage() {
  useFacebookSDK();

  return (
    <>
      <div
        className="fb-page"
        data-href="https://www.facebook.com/MW-Store-100730118877099/"
        data-width="340"
        data-height="140"
        data-small-header="false"
        data-adapt-container-width="true"
        data-hide-cover="false"
        data-show-facepile="false"
      >
        <blockquote
          cite="https://www.facebook.com/MW-Store-100730118877099/"
          className="fb-xfbml-parse-ignore"
        >
          <a href="https://www.facebook.com/MW-Store-100730118877099/">
            MW Store
          </a>
        </blockquote>
      </div>
    </>
  );
}

export default FacebookPage;
