import React from "react";
import useFacebookSDK from "~/hooks/useFacebookSDK";

const pageLink = window.location.href;

function FacebookComment() {
  useFacebookSDK();

  return (
    <>
      <div
        className="fb-comments"
        data-href={pageLink}
        data-width="800"
        data-numposts="20"
      ></div>
    </>
  );
}

export default FacebookComment;
