import React, { useState } from "react";
import { Button } from "antd";
import toast from "~/helpers/toast";
import config from "~/constants";
import couponApi from "~/apis/couponApi";

function Coupon(props) {
  const { setCoupon, couponCode, setCouponCode } = props;
  const [confirmLoading, setConfirmLoading] = useState(false);

  const checkCoupon = () => {
    if (couponCode === "") {
      setCoupon({ code: "NO", money: 0 });
      return toast.warning("Thất bại", "Vui lòng nhập mã giảm giá");
    }

    setConfirmLoading(true);
    couponApi
      .useCoupon({ code: couponCode })
      .then((res) => {
        setConfirmLoading(false);
        if (res.status === config.response.SUCCESS) {
          const { code, money } = res.data;
          setCoupon({ code, money });
          return toast.success("Thành công", "Mã giảm giá đã được áp dụng");
        }

        setCoupon({ code: "NO", money: 0 });
        if (res.status === config.response.FAIL) {
          if (res.message === "NOT_FOUND") {
            return toast.warning(
              "Thất bại",
              "Mã giảm giá không hợp lệ hoặc không tồn tại"
            );
          }
          if (res.message === config.response.HAS_USED) {
            return toast.warning("Thất bại", "Mã giảm giá đã được sử dụng");
          }
        }
      })
      .catch((err) => {
        setCoupon({ code: "NO", money: 0 });
        setConfirmLoading(false);

        return toast.warning("Thất bại", "Vui lòng thử lại");
      });
  };
  return (
    <div className="coupon-section">
      <h4 className="choose-address">Mã giảm giá</h4>
      <div className="coupon-content">
        <input
          type="text"
          className="form-control"
          onChange={(e) => setCouponCode(e.target.value)}
          placeholder="Nhập mã giảm giá..."
        />
        <Button
          className="btn btn-coupon"
          loading={confirmLoading}
          onClick={checkCoupon}
        >
          Áp dụng
        </Button>
      </div>
    </div>
  );
}

export default Coupon;
