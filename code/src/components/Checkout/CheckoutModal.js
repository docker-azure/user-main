import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";
import { Modal, Button } from "antd";
import { useFormik } from "formik";
import io from "socket.io-client";
import config from "~/constants";
import toast from "~/helpers/toast";
import orderApi from "~/apis/orderApi";
import { getCart } from "~/actions/action";
import { checkoutValid } from "~/helpers/validate";

const socket = io(config.api.BASE_URL_NODE);

function CheckoutModal(props) {
  const { openModal, toggleModal, method, coupon, feeship, address } = props;
  const [confirmLoading, setConfirmLoading] = useState(false);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userReducer.user);
  const [redirectOrder, setRedirectOrder] = useState(false);
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: user.name || "",
      phone: user.phone || "",
      note: "",
    },
    validationSchema: checkoutValid,
    onSubmit: (value) => {
      setConfirmLoading(true);
      const values = {
        ...value,
        method,
        coupon_code: coupon.code,
        feeship,
        address,
      };

      orderApi
        .newOrder(values)
        .then((res) => {
          console.log("res: ", res);
          // return;
          if (res.status === config.response.SUCCESS) {
            const { order, url_paypal } = res.data;

            if (method === 0) {
              toast.success("Thành công", "Đặt hàng thành công");
              dispatch(getCart());
              socket.emit("send_notification", {
                notification: `${user.name} has just added one more order`,
              });
              return setRedirectOrder(true);
            }

            if (method === 1) {
              orderApi
                .orderVNPay(order)
                .then((resVNPay) => {
                  window.location.href = resVNPay.data.url;
                })
                .catch((err) => console.log(err));
            }

            if (method === 2) {
              orderApi
                .orderMomo(order)
                .then((resMomo) => {
                  console.log(resMomo);
                  window.location.href = resMomo.data.url;
                })
                .catch((err) => console.log(err));
            }

            if (method === 3) {
              window.location.href = url_paypal;
            }
          } else {
            setConfirmLoading(false);
            toast.error("Thất bại", "Thanh toán thất bại");
          }
        })
        .catch((err) => {
          setConfirmLoading(false);
          toast.error("Thất bại", "Thanh toán thất bại");
        });
    },
  });

  if (redirectOrder) {
    return <Redirect to={config.routes.ORDER} />;
  }

  return (
    <Modal visible={openModal} onCancel={toggleModal} footer={null}>
      <div className="form-section">
        <h3 className="user-info">Thông tin khách hàng</h3>
        <form onSubmit={formik.handleSubmit} className="form-info">
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-user" />
              <input
                type="text"
                name="name"
                className="form-control"
                value={formik.values.name}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Tên người nhận..."
              />
            </div>
            {formik.errors.name && formik.touched.name && (
              <p className="error">{formik.errors.name}</p>
            )}
          </div>
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-mobile" />
              <input
                type="text"
                name="phone"
                className="form-control"
                value={formik.values.phone}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Số điện thoại người nhận..."
              />
            </div>
            {formik.errors.phone && formik.touched.phone && (
              <p className="error">{formik.errors.phone}</p>
            )}
          </div>

          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-sticky-note"></i>
              <input
                type="text"
                name="note"
                className="form-control"
                value={formik.values.note}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Ghi chú nếu có..."
              />
            </div>
            {formik.errors.note && formik.touched.note && (
              <p className="error">{formik.errors.note}</p>
            )}
          </div>

          <div className="order-button">
            <Button
              className="btn btn-order custom"
              loading={confirmLoading}
              htmlType="submit"
            >
              Đặt hàng
            </Button>
          </div>
        </form>
      </div>
    </Modal>
  );
}

export default CheckoutModal;
