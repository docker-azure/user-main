import React from "react";
import { Radio } from "antd";
import cashIcon from "~/assets/img/payment/cash.png";
import momoIcon from "~/assets/img/payment/momo.png";
import vnpayIcon from "~/assets/img/payment/vnpay.png";
import paypalIcon from "~/assets/img/payment/paypal.png";

function Payment(props) {
  const { payment, setPayment } = props;

  return (
    <div className="payment-method">
      <h4 className="choose-address">Phương thức thanh toán</h4>
      <ul className="method-list">
        <Radio.Group
          value={payment}
          onChange={(e) => setPayment(e.target.value)}
        >
          <div className="method-box">
            <li className="method-item">
              <Radio value={0}>
                <img src={cashIcon} alt="momo" /> Tiền mặt
              </Radio>
            </li>
            <li className="method-item">
              <Radio value={1}>
                <img src={vnpayIcon} alt="momo" /> Vn Pay
              </Radio>
            </li>
          </div>
          <div className="method-box">
            <li className="method-item">
              <Radio value={2}>
                <img src={momoIcon} alt="momo" /> Momo
              </Radio>
            </li>
            <li className="method-item">
              <Radio value={3}>
                <img src={paypalIcon} alt="momo" /> Paypal
              </Radio>
            </li>
          </div>
        </Radio.Group>
      </ul>
    </div>
  );
}

export default Payment;
