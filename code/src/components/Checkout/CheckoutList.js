import React from "react";
import CheckoutItem from "./CheckoutItem";

function CheckoutList(props) {
  const { carts } = props;

  return (
    <table className="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Hình ảnh</th>
          <th>Tên sản phẩm</th>
          <th>Số lượng</th>
          <th>Giá</th>
          <th>Tổng tiền</th>
        </tr>
      </thead>
      <tbody>
        {carts.map((cart, index) => (
          <CheckoutItem key={cart.id} cart={cart} index={index + 1} />
        ))}
      </tbody>
    </table>
  );
}

export default CheckoutList;
