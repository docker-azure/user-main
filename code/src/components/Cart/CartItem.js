import React, { useRef, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { Checkbox, Popconfirm, Button } from "antd";
import config from "~/constants";
import { getCart, checkedCart } from "~/actions/action";
import { formatPrice } from "~/helpers/helpers";
import cartApi from "~/apis/cartApi";
import toast from "~/helpers/toast";

function CartItem(props) {
  const { cart } = props;
  const { product } = cart;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const typingTimeoutRef = useRef(null);
  const [isPopVisible, setIsPopVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [isChecked, setIsChecked] = useState(false);

  useEffect(() => {
    if (cart.checked === "CHECK") {
      setIsChecked(true);
    }
    if (cart.checked === "UN_CHECK") {
      setIsChecked(false);
    }
  }, [cart]);

  function onChange(e) {
    if (e.target.checked) {
      dispatch(checkedCart({ id: cart.id, type: "CHECK" }));
      setIsChecked(true);
    } else {
      dispatch(checkedCart({ id: cart.id, type: "UN_CHECK" }));
      setIsChecked(false);
    }
  }

  const showPopconfirm = () => {
    setIsPopVisible(true);
  };

  const handlePopOk = () => {
    setConfirmLoading(true);
    const values = { id: cart.id };

    cartApi
      .deleteCart(values)
      .then((res) => {
        setIsPopVisible(false);
        setConfirmLoading(false);

        if (res.status === config.response.SUCCESS) {
          dispatch(getCart());
          return toast.success(
            t("common.success"),
            t("cart.delete_product_successfully")
          );
        }
        return toast.error(t("common.fail"), t("cart.delete_product_fail"));
      })
      .catch((err) => {
        setIsPopVisible(false);
        setConfirmLoading(false);
        return toast.error(t("common.fail"), t("cart.delete_product_fail"));
      });
  };

  const handlePopCancel = () => {
    setIsPopVisible(false);
  };

  const changeQuantity = (e) => {
    const updateQuantity = () => {
      let { value } = e.target;
      if (value === "" || parseInt(value) === 0 || value < 1 || value > 99)
        value = 1;

      const values = {
        product_id: cart.product.id,
        quantity: value,
      };

      cartApi
        .newCart(values)
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            dispatch(getCart());
            return toast.success(
              "Thành công",
              "Cập nhập số lượng thành công !!!"
            );
          }

          return toast.warning("Thất bại", "Cập nhập số lượng thất bại");
        })
        .catch((err) => {
          return toast.warning("Thất bại", "Cập nhập số lượng thất bại");
        });
    };

    if (typingTimeoutRef.current) {
      clearTimeout(typingTimeoutRef.current);
    }

    typingTimeoutRef.current = setTimeout(() => {
      updateQuantity();
    }, 800);
  };

  return (
    <tr>
      <td>
        <Checkbox onChange={onChange} checked={isChecked} />
      </td>
      <td>
        <Link to={config.routes.PRODUCT + product.slug} className="pro-img">
          <img
            src={config.api.PRODUCT_IMAGE + product.image}
            alt={product.name}
          />
        </Link>
      </td>
      <td>
        <Link to={config.routes.PRODUCT + product.slug} className="pro-name">
          {product.name}
        </Link>
      </td>
      <td>
        <input
          type="number"
          className="pro-quantity"
          defaultValue={cart.quantity}
          onKeyPress={(event) => {
            if (!/[0-9]/.test(event.key)) {
              event.preventDefault();
            }
          }}
          onBlur={(e) => {
            if (e.target.value < 1 || e.target.value > 99) {
              e.target.value = 1;
            }
          }}
          onChange={changeQuantity}
        />
      </td>
      <td>{formatPrice(product.price)}</td>
      <td>{formatPrice(product.price * cart.quantity)}</td>
      <td>
        <Popconfirm
          title="Bạn có thật sự muốn xóa ?"
          visible={isPopVisible}
          onConfirm={handlePopOk}
          okButtonProps={{ loading: confirmLoading }}
          onCancel={handlePopCancel}
          okText={t("common.yes")}
          cancelText={t("common.no")}
        >
          <Button
            type="dashed"
            danger
            style={{ backgroundColor: "transparent" }}
            onClick={showPopconfirm}
          >
            {t("common.delete")}
          </Button>
        </Popconfirm>
      </td>
    </tr>
  );
}

export default CartItem;
