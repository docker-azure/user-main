import React, { useState } from "react";
import { Rate, Button } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import toast from "~/helpers/toast";
import cartApi from "~/apis/cartApi";
import config from "~/constants";
import { getCart } from "~/actions/action";
import { formatPrice } from "~/helpers/helpers";
import FacebookLikeShare from "~/components/Facebook/FacebookLikeShare";

function ProductInfo(props) {
  const dispatch = useDispatch();
  const { product, changeQuantity, setQuantity, comments, quantity } = props;

  const { t } = useTranslation();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const isAuth = useSelector((state) => state.userReducer.isAuth);

  const addToCart = () => {
    if (!isAuth) {
      return toast.warning(
        t("common.fail"),
        t("product.sign_in_to_add_to_cart")
      );
    }

    setConfirmLoading(true);

    const values = { product_id: product.id, quantity };
    cartApi
      .newCart(values)
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(getCart());
          setConfirmLoading(false);

          return toast.success(
            t("common.success"),
            t("product.add_to_cart_success")
          );
        }
        setConfirmLoading(false);
        return toast.warning(t("common.fail"), t("product.add_to_cart_fail"));
      })
      .catch((err) => {
        setConfirmLoading(false);
        return toast.warning(t("common.fail"), t("product.add_to_cart_fail"));
      });
  };

  return (
    <div className="pro-info">
      <h3 className="pro-name">{product.name}</h3>
      <div className="pro-share">
        <FacebookLikeShare />
      </div>
      <p className="pro-view">
        {t("common.view")}: {product.visit} <i className="fas fa-eye" />
      </p>
      <div className="rating">
        <Rate
          disabled
          character={<i className="fas fa-star" />}
          value={product.star}
        />
      </div>
      <div className="pro-price">
        <div className="pro-price-current">{formatPrice(product.price)}</div>
        <del className="pro-price-prev">
          {formatPrice(product.price - 1000000)}
        </del>
      </div>
      <div className="quantity-box">
        <p className="quantity-text">{t("common.quantity")}: </p>
        <div className="quantity-action">
          <span className="sub" onClick={() => changeQuantity(-1)}>
            <i className="fas fa-angle-left" />
          </span>
          <input
            type="text"
            className="quantity-number"
            min={1}
            max={99}
            value={quantity}
            onChange={(e) => {
              if (e.target.value <= 99 && e.target.value >= 0) {
                setQuantity(e.target.value);
              }
            }}
            onBlur={() => {
              if (quantity < 1 || quantity > 99) {
                setQuantity(1);
              }
            }}
          />
          <span className="add" onClick={() => changeQuantity(1)}>
            <i className="fas fa-angle-right" />
          </span>
        </div>
      </div>
      <Button
        className="btn add-cart"
        loading={confirmLoading}
        onClick={addToCart}
      >
        {t("product.add_to_cart")}
      </Button>
      <div className="rating-feedback">
        {t("product.review_product", { review: comments.length })}
      </div>
    </div>
  );
}

export default ProductInfo;
