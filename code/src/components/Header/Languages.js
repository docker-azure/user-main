import React from "react";
import clsx from "clsx";
import i18n from "~/translation/i18n";

function Languages() {
  const language = i18n.language;

  const handleChangeLanguage = (lang) => {
    i18n.changeLanguage(lang);
    localStorage.setItem("lang", lang);
  };

  return (
    <div className="languages">
      <span
        className={clsx("lang-vi", { active: language === "vi" })}
        onClick={() => handleChangeLanguage("vi")}
      >
        VN
      </span>
      <span
        className={clsx("lang-en", { active: language === "en" })}
        onClick={() => handleChangeLanguage("en")}
      >
        EN
      </span>
    </div>
  );
}

export default Languages;
