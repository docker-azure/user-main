import React from "react";
import { Link, useRouteMatch } from "react-router-dom";

function CustomLink(props) {
  const { to, children, exact } = props;
  const match = useRouteMatch({ path: to, exact: exact });

  return (
    <li className={`navbar-item ${match && to !== "" ? "active" : ""}`}>
      {to === "" ? (
        <div className="navbar-item-link">{children}</div>
      ) : (
        <Link to={to} className="navbar-item-link">
          {children}
        </Link>
      )}
    </li>
  );
}

export default CustomLink;
