import React, { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { Link, useHistory } from "react-router-dom";
import config from "~/constants";
import toast from "~/helpers/toast";
import Languages from "./Languages";
import NavMobile from "./NavMobile";
import CustomLink from "./CustomLink";
import MWStoreLogo from "~/assets/img/logo/mwstore.png";
import {
  signOutReq,
  signOut,
  deleteAllCart,
  deleteAllOrder,
} from "~/actions/action";

function Header() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation();
  const [isToggleNav, setIsToggleNav] = useState(false);
  const isAuth = useSelector((state) => state.userReducer.isAuth);
  const user = useSelector((state) => state.userReducer.user);
  const carts = useSelector((state) => state.cartReducer.carts);
  const header = useRef();

  useEffect(() => {
    let topPage = 0;

    window.addEventListener("scroll", () => {
      const topPageOld = window.pageYOffset;

      try {
        if (topPageOld > topPage)
          header.current.style.animation = "headerOut ease-in 0.4s";
        else header.current.style.animation = "headerIn ease-in 0.4s";

        header.current.style.animationFillMode = "forwards";
        topPage = topPageOld;
      } catch (error) {}
    });
  }, []);

  const handleLogout = async () => {
    const signout = () => {
      dispatch(signOutReq());
      dispatch(signOut());
      dispatch(deleteAllCart());
      dispatch(deleteAllOrder());
    };

    await signout();
    toast.success(t("common.success"), t("header.sign_out_success"));
    history.replace(config.routes.SIGN_IN);
  };

  const toggleNav = () => {
    setIsToggleNav(!isToggleNav);
  };

  return (
    <header className="header" ref={header}>
      <NavMobile isToggleNav={isToggleNav} setIsToggleNav={setIsToggleNav} />
      <div className="navbar">
        <Link to={config.routes.HOME} className="navbar-logo">
          <img src={MWStoreLogo} alt="mwstore logo" />
        </Link>
        <div className="navbar-content">
          <Languages />
          <Link to={config.routes.SEARCH} className="search-box">
            <i className="open-search fas fa-search" />
          </Link>

          <ul className="navbar-list">
            <CustomLink to={config.routes.HOME} exact>
              {t("header.home")}
            </CustomLink>
            <CustomLink to={config.routes.CART}>
              {t("header.cart")}
              {carts && carts.length > 0 && (
                <span className="cart-quantity">{carts.length}</span>
              )}
            </CustomLink>
            <CustomLink to={config.routes.ORDER}>
              {t("header.order")}
            </CustomLink>
            <CustomLink to={config.routes.NEWS}>{t("header.news")}</CustomLink>
            <CustomLink to={config.routes.CONTACT}>
              {t("header.contact")}
            </CustomLink>
            {/* <CustomLink to="">
              {t("header.utilities")} <i className="fas fa-angle-down" />
              <ul className="dropdown-list">
                <li className="dropdown-item">
                  <Link to={config.routes.YOUTUBE_SEARCH} className="dropdown-item-link">
                    Youtube search
                  </Link>
                </li>
              </ul>
            </CustomLink> */}
          </ul>

          <div className="navbar-user">
            {!isAuth ? (
              <Link to={config.routes.SIGN_IN} className="user-auth">
                <i className="fas fa-user-shield"></i>
              </Link>
            ) : (
              <>
                <div className="user-avatar">
                  <img
                    src={config.api.AVATAR_IMAGE + user.image}
                    alt={user.name}
                  />
                </div>
                <p className="user-name">
                  {user.name} <i className="fas fa-angle-down" />
                </p>
                <ul className="dropdown-list">
                  <li className="dropdown-item">
                    <Link
                      to={config.routes.PROFILE}
                      className="dropdown-item-link"
                    >
                      {t("header.info")}
                    </Link>
                  </li>
                  {/* <li className="dropdown-item">
                    <Link to={config.routes.ACTIVITY} className="dropdown-item-link">
                      {t("header.activity")}
                    </Link>
                  </li> */}
                  <li className="dropdown-item">
                    <Link
                      to=""
                      className="dropdown-item-link"
                      onClick={handleLogout}
                    >
                      {t("header.sign_out")}
                    </Link>
                  </li>
                </ul>
              </>
            )}
          </div>
          <div className="navbar-button-open" onClick={toggleNav}>
            <i className="fas fa-bars" />
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
