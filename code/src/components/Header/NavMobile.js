import React, { useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import config from "~/constants";
import CustomLinkMobile from "./CustomLinkMobile";

const body = document.body;

function NavbarMobile(props) {
  const { isToggleNav, setIsToggleNav } = props;
  const { t } = useTranslation();
  const overlay = useRef();
  const navSide = useRef();

  useEffect(() => {
    if (isToggleNav) {
      toggleNav();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isToggleNav]);

  const toggleNav = () => {
    if (!overlay.current.classList.contains("active")) {
      body.style.overflow = "hidden";
      overlay.current.classList.toggle("active");
      navSide.current.style.animation = "navMobieFadeIn 1s";
    } else {
      navSide.current.style.animation = "navMobieFadeOut 1s";
      setTimeout(() => {
        overlay.current.classList.toggle("active");
      }, 1000);
      body.style.overflow = "auto";
      setIsToggleNav(!isToggleNav);
    }

    navSide.current.style.animationFillMode = "forwards";
  };

  return (
    <>
      <div className="overlay" onClick={toggleNav} ref={overlay}></div>
      <div className="navbar-side" ref={navSide}>
        <div className="navbar-side-header">
          <h4 className="navbar-side-name">Danh mục</h4>
          <div className="navbar-button-close" onClick={toggleNav}>
            <i className="fas fa-times" />
          </div>
        </div>
        <ul className="navbar-side-list">
          <CustomLinkMobile
            exact
            to={config.routes.HOME}
            name={t("header.home")}
            icon={<i className="fas fa-home"></i>}
          />

          <CustomLinkMobile
            to={config.routes.SEARCH}
            name={t("header.search")}
            icon={<i className="fas fa-search"></i>}
          />

          <CustomLinkMobile
            to={config.routes.CART}
            name={t("header.cart")}
            icon={<i className="fas fa-shopping-cart"></i>}
          />

          <CustomLinkMobile
            to={config.routes.ORDER}
            name={t("header.order")}
            icon={<i className="fas fa-shipping-fast"></i>}
          />

          <CustomLinkMobile
            to={config.routes.CONTACT}
            name={t("header.contact")}
            icon={<i className="fas fa-address-card"></i>}
          />
        </ul>
      </div>
    </>
  );
}

export default NavbarMobile;
