import axiosClient from "./axiosClient";
import config from "~/constants";

const sliderApi = {
  getAll: () => {
    return axiosClient.get(config.api.GET_SLIDER);
  },
};

export default sliderApi;
