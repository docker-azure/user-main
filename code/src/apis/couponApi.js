import axiosClient from "./axiosClient";
import config from "~/constants";

const couponApi = {
  useCoupon: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.USE_COUPON, value);
    });
  },
};

export default couponApi;
