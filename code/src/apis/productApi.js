import axiosClient from "./axiosClient";
import config from "~/constants";

const productApi = {
  get: (slug) => {
    return axiosClient.get(`${config.api.GET_PRODUCT}${slug}`);
  },
  getProductSearch: (filters) => {
    return axiosClient.post(config.api.PRODUCT_SEARCH, filters);
  },
  getProductBrand: (brandId) => {
    return axiosClient.get(`${config.api.PRODUCT_BRAND}${brandId}`);
  },
  getProductNew: () => {
    return axiosClient.get(config.api.PRODUCT_NEW);
  },
  getProductMore: (params) => {
    return axiosClient.get(config.api.PRODUCT_MORE, { params });
  },
  getProductFeather: () => {
    return axiosClient.get(config.api.PRODUCT_FEATHER);
  },
  updateView: (slug) => {
    return axiosClient.get(`${config.api.UPDATE_VIEW}${slug}`);
  },
};

export default productApi;
