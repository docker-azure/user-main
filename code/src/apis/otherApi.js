import axios from "axios";
import axiosClient, { axiosInstance } from "./axiosClient";
import config from "~/constants";

const otherApi = {
  youtubeSearch: (params) => {
    return axiosClient.get(config.api.YOUTUBE_SEARCH, {
      params,
      withCredentials: false,
    });
  },
  convertMoney: () => {
    return axios.get(config.api.CONVERT_MONEY);
  },
  getRssFeed: (key) => {
    return axios.get(config.api.RSS_FEED + key);
  },

  cacheFacebook: (url) => {
    return axiosInstance.post(config.api.FB_GRAPH, {
      id: url,
      scrape: true,
      access_token: config.envConst.FB_TOKEN,
    });
  },
};

export default otherApi;
