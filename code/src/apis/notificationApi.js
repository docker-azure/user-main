import axiosClient from "./axiosClient";
import config from "~/constants";

const notificationApi = {
  saveTokenNotification: (data) => {
    return axiosClient.post(config.api.SAVE_TOKEN_NOTIFICATION, data);
  },
};

export default notificationApi;
