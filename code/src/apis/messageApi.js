import axiosClient from "./axiosClient";
import config from "~/constants";

const messageApi = {
  getAllMessage: (data) => {
    return axiosClient.post(config.api.ALL_MESSAGE, data);
  },

  newMessage: (data) => {
    return axiosClient.post(config.api.NEW_MESSAGE, data);
  },
};

export default messageApi;
