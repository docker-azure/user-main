export const BASE_URL = process.env.REACT_APP_BASE_URL_SERVER;
export const BASE_URL_NODE = process.env.REACT_APP_BASE_URL_NODE;
export const GHN_TOKEN = process.env.REACT_APP_GHN_TOKEN;
export const FB_TOKEN = process.env.REACT_APP_FB_TOKEN;
export const CURRENCY_CONVERTER_KEY =
  process.env.REACT_APP_CURRENCY_CONVERTER_KEY;
