import { useEffect, useRef } from "react";
import { io } from "socket.io-client";
import toast from "~/helpers/toast";
import config from "~/constants";

function useNotification() {
  const socket = useRef();

  useEffect(() => {
    socket.current = io(config.api.BASE_URL_NODE);
    socket.current.on("receive_notification", (data) => {
      toast.success("Notification", data.notification);
    });
  }, []);
}

export default useNotification;
