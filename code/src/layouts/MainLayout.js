import React from "react";
import { useSelector } from "react-redux";
import useNotification from "~/hooks/useNotification";
import Header from "~/components/Header";
import Footer from "~/components/Footer";
import MusicPlayer from "~/components/Music/MusicPlayer";
import Notification from "~/components/Notification/Notification";
import ScrollTop from "~/components/ScrollTop";
import Chat from "~/components/Chat";
import Routes from "~/routes";
import "~/themes/General/antd.css";
import GlobalStyles from "~/themes/GlobalStyles";

function MainLayout() {
  const isAuth = useSelector((state) => state.userReducer.isAuth);

  useNotification();

  return (
    <GlobalStyles>
      <div className="app">
        <Header />
        <div className="content gird">
          <Routes />
        </div>
        <Footer />
        <Notification />
        <MusicPlayer />
        <ScrollTop />
        {isAuth && <Chat />}
      </div>
    </GlobalStyles>
  );
}

export default MainLayout;
