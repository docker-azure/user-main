import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import AuthLayout from "~/layouts/AuthLayout";

function App() {
  return (
    <Router>
      <AuthLayout />
    </Router>
  );
}

export default App;
