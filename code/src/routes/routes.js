import React from "react";
import { Route, Switch } from "react-router-dom";
import PrivateRoute from "~/guards/PrivateRoute";
import config from "~/constants";
import Home from "~/pages/Home";
import Product from "~/pages/Product";
import Cart from "~/pages/Cart";
import Order from "~/pages/Order";
import OrderDetail from "~/pages/OrderDetail";
import Checkout from "~/pages/Checkout";
import SignIn from "~/pages/SignIn";
import SocialLogin from "~/pages/SocialLogin";
import Payment from "~/pages/Payment";
import SignUp from "~/pages/SignUp";
import Search from "~/pages/Search";
import YoutubeSearch from "~/pages/YoutubeSearch";
import Profile from "~/pages/Profile";
import Activity from "~/pages/Activity";
import Contact from "~/pages/Contact";
import News from "~/pages/News";
import NotFound from "~/pages/NotFound";
import Test from "~/pages/Test";

function Routes() {
  return (
    <Switch>
      <Route path="/test" component={Test} />
      <Route path={config.routes.SIGN_UP} component={SignUp} />
      <Route exact path={config.routes.HOME} component={Home} />
      <Route path={config.routes.PRODUCT + ":slug"} component={Product} />
      <Route exact path={config.routes.SIGN_IN} component={SignIn} />
      <Route path={config.routes.SIGN_IN_SOCIAL_CB} component={SocialLogin} />
      <Route path={config.routes.SEARCH} component={Search} />
      <Route path={config.routes.YOUTUBE_SEARCH} component={YoutubeSearch} />
      <Route path={config.routes.CONTACT} component={Contact} />
      <Route path={config.routes.NEWS} component={News} />
      <PrivateRoute path={config.routes.PROFILE} component={Profile} />
      <PrivateRoute path={config.routes.ACTIVITY} component={Activity} />
      <PrivateRoute path={config.routes.CART} component={Cart} />
      <PrivateRoute path={config.routes.CHECKOUT} component={Checkout} />
      <PrivateRoute exact path={config.routes.ORDER} component={Order} />
      <PrivateRoute
        path={config.routes.ORDER_DETAIL + ":id"}
        component={OrderDetail}
      />
      <PrivateRoute path={config.routes.PAYMENT_CB} component={Payment} />
      <Route path="*" component={NotFound} />
      <Route path={config.routes.NOT_FOUND} component={NotFound} />
    </Switch>
  );
}

export default Routes;
