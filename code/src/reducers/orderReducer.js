import config from "~/constants";

const initialState = config.orderTab.reduce((states, state) => {
  const { key } = state;
  return {
    ...states,
    [key]: {
      orders: null,
    },
  };
}, {});

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.types.INIT_ORDER: {
      // const orders = action.payload;
      return {};
      break;
    }

    case config.types.FILTER_ORDER: {
      const { key, orders } = action.payload;
      return {
        ...state,
        [key]: {
          orders,
        },
      };
    }

    case config.types.DELETE_ALL_ORDER: {
      return {};
      break;
    }

    default:
      return state;
  }
};

export default orderReducer;
