import { combineReducers } from "redux";
import userReducer from "./userReducer";
import cartReducer from "./cartReducer";
import orderReducer from "./orderReducer";
import addressReducer from "./addressReducer";
import newsReducer from "./newsReducer";

const rootReducers = combineReducers({
  userReducer,
  cartReducer,
  orderReducer,
  addressReducer,
  newsReducer,
});

export default rootReducers;
