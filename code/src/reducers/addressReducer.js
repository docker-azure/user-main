import config from "~/constants";

const initialState = {
  address: [],
};

const addressReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.types.INIT_ADDRESS: {
      const address = action.payload;
      return { address };
    }

    default:
      return state;
  }
};

export default addressReducer;
