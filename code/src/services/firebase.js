import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyDU2edSdegoFs11a_DocHIp8oY9h0pFPv0",
  authDomain: "mwstore-notification.firebaseapp.com",
  databaseURL: "https://mwstore-notification-default-rtdb.firebaseio.com",
  projectId: "mwstore-notification",
  storageBucket: "mwstore-notification.appspot.com",
  messagingSenderId: "508025420900",
  appId: "1:508025420900:web:ae088a50c2a0b029307371",
  measurementId: "G-MMTX4705BM",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Cloud Messaging and get a reference to the service
const messaging = getMessaging(app);

export const requestPermission = () =>
  new Promise((resolve, reject) => {
    Notification.requestPermission()
      .then(() => getToken(messaging))
      .then((firebaseToken) => {
        resolve(firebaseToken);
      })
      .catch((err) => {
        reject(err);
      });
  });

export const onMessageListener = () =>
  new Promise((resolve) => {
    onMessage(messaging, (payload) => {
      const { title, body, icon, image, actions } = payload.notification;
      const options = { title, body, icon, image, actions };

      new Notification(title, options);

      resolve(payload);
    });
  });
