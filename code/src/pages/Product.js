import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { useParams, useHistory } from "react-router-dom";
import { isEmpty } from "lodash";
import { Helmet } from "react-helmet";
import config from "~/constants";
import productApi from "~/apis/productApi";
import commentApi from "~/apis/commentApi";
import ProductList from "~/components/Product/ProductList";
import Gallery from "~/components/Product/Gallery";
import ProductInfo from "~/components/Product/ProductInfo";
import TabDescription from "~/components/Product/TabDescription";
import WaveLoading from "~/components/Loading/WaveLoading";

function Product() {
  const history = useHistory();
  const { slug } = useParams();
  const { t } = useTranslation();
  const isAuth = useSelector((state) => state.userReducer.isAuth);
  const [product, setProduct] = useState({});
  const [gallerys, setGallerys] = useState([]);
  const [post, setPost] = useState({});
  const [comments, setComments] = useState([]);
  const [updateComment, setUpdateComment] = useState(false);
  const [yourComment, setYourComment] = useState([]);
  const [productBrands, setProductBrands] = useState([]);
  let [quantity, setQuantity] = useState(1);

  useEffect(() => {
    if (!isEmpty(product)) {
      productApi.updateView(product.slug);
    }
  }, [slug, product]);

  useEffect(() => {
    window.scrollTo(0, 0);
    setGallerys([]);
    setPost({});
    setComments([]);
    setYourComment([]);
    setProductBrands([]);
    setQuantity(1);
  }, [slug]);

  useEffect(() => {
    const fetchProduct = () => {
      productApi
        .get(slug)
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            const { gallerys, post } = res.data;

            setProduct(res.data);
            setPost(post);
            setGallerys(gallerys);
          } else {
            history.replace(config.routes.NOT_FOUND);
          }
        })
        .catch((err) => {
          history.replace(config.routes.NOT_FOUND);
        });
    };

    fetchProduct();
  }, [slug, history]);

  useEffect(() => {
    if (!isEmpty(product)) {
      productApi
        .getProductBrand(product.brand_id)
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            setProductBrands(res.data);
          }
        })
        .catch((err) => {});
    }
  }, [slug, product]);

  useEffect(() => {
    if (!isEmpty(product)) {
      commentApi
        .getProductComment({ product_id: product.id })
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            setComments(res.data);
          }
        })
        .catch((err) => {});
    }
  }, [product, updateComment]);

  useEffect(() => {
    setYourComment({});

    if (isAuth && product) {
      commentApi
        .getUserComment({ product_id: product.id })
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            setYourComment(res.data);
          }
        })
        .catch((err) => {});
    }
  }, [isAuth, product, updateComment]);

  const changeQuantity = (value) => {
    const newQuantity = quantity + value;
    if (newQuantity >= 1 && newQuantity <= 99) {
      setQuantity(newQuantity);
    }
  };

  const handleSetUpdate = () => {
    setUpdateComment(!updateComment);
  };

  return (
    <>
      {!isEmpty(product) ? (
        <>
          <Helmet>
            <title>{product.name}</title>
            <link rel="canonical" href={window.location.href} />
            <meta name="keywords" content={product.keyword} />
            <meta name="description" content={product.description} />

            <meta property="og:title" content={product.name} />
            <meta property="og:url" content={window.location.href} />
            <meta property="og:type" content="article" />
            <meta property="og:description" content={product.description} />
            <meta
              property="og:image"
              content={`${config.api.PRODUCT_IMAGE}${product.image}`}
            />

            <meta name="twitter:title" content={product.name} />
            <meta name="twitter:description" content={product.description} />
            <meta
              name="twitter:image"
              content={`${config.api.PRODUCT_IMAGE}${product.image}`}
            />
            <meta name="twitter:card" content="summary_large_image" />
          </Helmet>

          <div className="product-section row">
            <div className="col-12 col-lg-5 col-xl-5">
              <Gallery gallerys={gallerys} product={product} />
            </div>
            <div className="col-12 col-lg-7 col-xl-7">
              <ProductInfo
                product={product}
                changeQuantity={changeQuantity}
                setQuantity={setQuantity}
                comments={comments}
                quantity={quantity}
              />
            </div>
          </div>
          <div className="description-section row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <TabDescription
                product={product}
                post={post}
                comments={comments}
                yourComment={yourComment}
                handleSetUpdate={handleSetUpdate}
              />
            </div>
          </div>
          <h2 className="title-section">{t("product.product_related")}</h2>
          <ProductList products={productBrands} />
        </>
      ) : (
        <WaveLoading />
      )}
    </>
  );
}

export default Product;
