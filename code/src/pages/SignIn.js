import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { useFormik } from "formik";
import { Link, Redirect, useLocation } from "react-router-dom";
import useTitle from "~/hooks/useTitle";
import config from "~/constants";
import userApi from "~/apis/userApi";
import { signInValid } from "~/helpers/validate";
import SocialLink from "~/components/SocialLink";
import MWStoreLogo from "~/assets/img/logo/mwstore.png";
import WaveLoading from "~/components/Loading/WaveLoading";
import LoginImg from "~/assets/img/login/login.jpg";
import {
  signIn,
  signOut,
  getCart,
  // getOrder,
  getAddress,
  deleteAllCart,
  deleteAllOrder,
} from "~/actions/action";

function Login() {
  const { state } = useLocation();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [signinFail, setSigninFail] = useState(false);
  const [pressSignin, setPressSignin] = useState(false);
  const isAuth = useSelector((state) => state.userReducer.isAuth);
  const token = localStorage.getItem("token");

  useTitle(t("common.sign_in"));

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: signInValid,
    onSubmit: (values) => {
      setPressSignin(true);
      userApi
        .signIn(values)
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            dispatch(signIn(res.data));
            dispatch(getCart());
            // dispatch(getOrder());
            dispatch(getAddress());
            setPressSignin(false);
          } else {
            dispatch(signOut());
            dispatch(deleteAllCart());
            dispatch(deleteAllOrder());
            setSigninFail(true);
            setPressSignin(false);
          }
        })
        .catch((err) => {
          setPressSignin(false);
        });
    },
  });

  const focusInput = () => {
    setSigninFail(false);
  };

  if (isAuth || token) {
    return <Redirect to={state?.from || config.routes.HOME} />;
  }

  return (
    <div className="login-section row">
      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
        <div className="panel-left">
          <div className="login-img">
            <img src={LoginImg} alt="" />
          </div>
          <h3 className="has-account">{t("common.no_account")}</h3>
          <Link to={config.routes.SIGN_UP} className="btn-change">
            {t("common.sign_up")}
          </Link>
        </div>
      </div>
      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
        <div className="panel-right">
          <div className="logo-img">
            <img src={MWStoreLogo} alt="" />
          </div>
          <h2 className="title">{t("common.sign_in")}</h2>
          <form onSubmit={formik.handleSubmit} className="form-input">
            <div className="form-group">
              <div className="input-field">
                <i className="fas fa-envelope" />
                <input
                  type="text"
                  name="email"
                  className="form-control"
                  value={formik.values.email}
                  onFocus={focusInput}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  placeholder={t("common.input_email")}
                />
              </div>
              {formik.errors.email && formik.touched.email && (
                <p className="error">{formik.errors.email}</p>
              )}
            </div>
            <div className="form-group">
              <div className="input-field">
                <i className="fas fa-key" />
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  value={formik.values.password}
                  onFocus={focusInput}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  placeholder={t("common.input_password")}
                />
              </div>
              {formik.errors.password && formik.touched.password && (
                <p className="error">{formik.errors.password}</p>
              )}
            </div>

            <div className="login-action">
              {signinFail && (
                <p className="login-fail">
                  {t("common.wrong_email_or_password")}
                </p>
              )}

              {pressSignin && (
                <div style={{ marginTop: "10px" }}>
                  <WaveLoading />
                </div>
              )}

              <input
                type="submit"
                disabled={pressSignin}
                className="btn-submit"
                value={t("common.sign_in")}
              />
              <SocialLink />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
