import React from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import useTitle from "~/hooks/useTitle";
import { contactValid } from "~/helpers/validate";

function Contact() {
  const { t } = useTranslation();

  useTitle(t("header.contact"));

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    validationSchema: contactValid,
    onSubmit: (value) => {},
  });

  return (
    <div className="contact-page">
      <h3 className="title-section">{t("header.contact")}</h3>
      <div className="row">
        <div className="col-12 col-md-6 col-lg-6 col-xl-6">
          <div className="contact-left" style={{ height: "100%" }}>
            <div id="map" style={{ height: "100%" }}>
              <iframe
                title="map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.650597055417!2d106.67998241462075!3d10.761388462414164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f1b8a072901%3A0x2fb4502ebd044212!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBTxrAgcGjhuqFtIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1622878056287!5m2!1svi!2s"
                width="100%"
                height="100%"
                style={{ border: 0 }}
                allowFullScreen
                loading="lazy"
              />
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-6 col-xl-6">
          <div className="contact-right">
            <div className="form-section">
              <form onSubmit={formik.handleSubmit} className="form-info">
                <div className="form-group">
                  <div className="input-field">
                    <i className="fas fa-user" />
                    <input
                      type="text"
                      name="name"
                      className="form-control"
                      value={formik.values.name}
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      placeholder={t("common.input_name")}
                    />
                  </div>
                  {formik.errors.name && formik.touched.name && (
                    <p className="error">{formik.errors.name}</p>
                  )}
                </div>
                <div className="form-group">
                  <div className="input-field">
                    <i className="fas fa-envelope" />
                    <input
                      type="text"
                      name="email"
                      className="form-control"
                      value={formik.values.email}
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      placeholder={t("common.input_email")}
                    />
                  </div>
                  {formik.errors.email && formik.touched.email && (
                    <p className="error">{formik.errors.email}</p>
                  )}
                </div>

                <div className="form-group">
                  <div className="input-field">
                    <i className="fas fa-comment-alt message-icon"></i>
                    <textarea
                      type="text"
                      name="message"
                      rows={7}
                      className="form-control"
                      value={formik.values.message}
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      placeholder={t("common.input_content")}
                    ></textarea>
                  </div>
                  {formik.errors.message && formik.touched.message && (
                    <p className="error">{formik.errors.message}</p>
                  )}
                </div>
                <div className="contact-button-wrap">
                  <button className="btn btn-contact" type="submit">
                    {t("common.send_feedback")}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contact;
