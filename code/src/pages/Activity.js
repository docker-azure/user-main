import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import { Button } from "antd";
import useTitle from "~/hooks/useTitle";
import activityApi from "~/apis/activityApi";

function Activity() {
  const user = useSelector((state) => state.userReducer.user);
  const [activities, setActivities] = useState([]);
  const [noMore, setNoMore] = useState(true);
  const [loading, setLoading] = useState(false);
  const [pageLoad, setPageLoad] = useState({ page: 1, lastPage: 1 });
  useTitle("Lịch sử hoạt động");

  const notiEndRef = useRef(null);

  const scrollToBottom = () => {
    notiEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const fetchActivityMore = () => {
    const { page, lastPage } = pageLoad;

    if (page <= lastPage) {
      setLoading(true);
      activityApi
        .getActivity({ page, user_id: user.id })
        .then((res) => {
          const result = res.data;

          if (page <= result.last_page) {
            setPageLoad({
              page: page + 1,
              lastPage: result.last_page,
            });
          }

          setActivities([...activities, ...result.data]);
          setLoading(false);
          scrollToBottom();
        })
        .catch((err) => {
          setLoading(false);
        });
    } else {
      setNoMore(false);
    }
  };

  useEffect(() => {
    const { page, lastPage } = pageLoad;

    if (page > lastPage) setNoMore(false);
  }, [pageLoad]);

  useEffect(() => {
    fetchActivityMore();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="profile-page">
      <h3 className="title-section">Thông tin hoạt động</h3>
      <div className="row justify-content-center">
        <div className="profile">
          <div className="profile-body">
            {activities.map((activitie, key) => (
              <div key={key} className="info-item">
                <div className="info-icon">
                  <i className="fas fa-info-circle"></i>
                </div>
                <div className="info-left">
                  <h4 className="info-activity">{activitie.user_content}</h4>
                </div>
              </div>
            ))}

            <div className="scroll-to" ref={notiEndRef} />
          </div>
          <div className="profile-edit">
            <Button
              type="primary"
              htmlType="submit"
              size="large"
              disabled={!noMore}
              loading={loading}
              onClick={noMore ? fetchActivityMore : null}
            >
              {noMore ? "Load more activity" : "Full activity"}
            </Button>    
          </div>
        </div>
      </div>
    </div>
  );
}

export default Activity;
