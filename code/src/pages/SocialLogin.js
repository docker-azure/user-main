import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { capitalize } from "lodash";
import { Redirect, useLocation, useParams } from "react-router-dom";
import useTitle from "~/hooks/useTitle";
import config from "~/constants";
import userApi from "~/apis/userApi";
import toast from "~/helpers/toast";
import WaveLoading from "~/components/Loading/WaveLoading";
import {
  signIn,
  signOut,
  getCart,
  // getOrder,
  getAddress,
} from "~/actions/action";

function SocialLogin() {
  const dispatch = useDispatch();
  const location = useLocation();
  const { t } = useTranslation();
  const { social } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [redirectHome, setRedirectHome] = useState(false);

  useTitle(`${t("common.sign_in_with")} ${capitalize(social)}`);

  useEffect(() => {
    const providerSignin = social.toUpperCase();

    userApi
      .signInSocialCb(providerSignin + location.search)
      .then((res) => {
        console.log(res);
        if (res.status === config.response.SUCCESS) {
          dispatch(signIn(res.data));
          dispatch(getCart());
          // dispatch(getOrder());
          dispatch(getAddress());
          toast.success(t("common.success"), t("common.sign_in_success"));
          setIsLoading(false);
          setRedirectHome(true);
        } else {
          dispatch(signOut());
          setIsLoading(false);
        }
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
      });
  }, [dispatch, social, location.search, t]);

  if (redirectHome) {
    return <Redirect to={config.routes.HOME} />;
  }

  return (
    <div>
      {isLoading ? (
        <>
          <WaveLoading />
        </>
      ) : (
        <>
          {redirectHome ? (
            <p>{t("common.sign_in_success")}</p>
          ) : (
            <p>{t("common.sign_in_fail")}</p>
          )}
        </>
      )}
    </div>
  );
}

export default SocialLogin;
