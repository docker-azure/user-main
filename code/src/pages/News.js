import React, { useState, useEffect, useRef } from "react";
import className from "classnames/bind";
import { useSelector, useDispatch } from "react-redux";
import { Tabs } from "antd";
import moment from "moment";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import "moment/locale/vi";
import config from "~/constants";
import { getRssFeed } from "~/actions/action";
import styles from "~/themes/News/News.module.scss";

const rssFeed = config.rssFeed;

const cx = className.bind(styles);

const { TabPane } = Tabs;

moment.locale("vi");

function News() {
  const dispatch = useDispatch();
  const news = useSelector((state) => state.newsReducer);
  const [currentTab, setCurrentTab] = useState(rssFeed[0].key);
  const visitedTabs = useRef([]);

  useEffect(() => {
    const isVisitedTab = visitedTabs.current.includes(currentTab);
    if (!isVisitedTab) {
      visitedTabs.current = [...visitedTabs.current, currentTab];
      dispatch(getRssFeed(currentTab));
    }
  });

  const onChange = (key) => {
    setCurrentTab(key);
  };

  return (
    <>
      <h3 className="title-section">Tin tức</h3>
      <Tabs defaultActiveKey={currentTab} onChange={onChange}>
        {rssFeed.map((item) => {
          const { key, name } = item;

          return (
            <TabPane tab={name} key={key} className="content-desciption">
              {news[key] && (
                <div className="container">
                  {news[key].items.map((item, index) => {
                    const { title, img, date, desc, url } = item;

                    return (
                      <div className={cx("row", "news")} key={index}>
                        <a target="_blank" rel="noreferrer" href={url}>
                          <img className="" src={img} alt="" />
                        </a>
                        <div className={cx("box-news-content")}>
                          <a
                            target="_blank"
                            rel="noreferrer"
                            className={cx("news-title")}
                            href={url}
                          >
                            {title}
                          </a>
                          <p className={cx("news-content")}>{desc}</p>
                          <div className={cx("news-info")}>
                            <p className={cx("news-info-item")}>
                              {moment(date).format("LLLL")}
                            </p>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              )}

              {!news[key] && (
                <Skeleton
                  count={5}
                  height={"30px"}
                  duration={1}
                  style={{ margin: "6px 0" }}
                />
              )}
            </TabPane>
          );
        })}
      </Tabs>
    </>
  );
}

export default News;
