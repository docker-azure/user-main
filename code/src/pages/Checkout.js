import React, { useState, useEffect } from "react";
import useTitle from "~/hooks/useTitle";
import useToggle from "~/hooks/useToggle";
import toast from "~/helpers/toast";
import cartApi from "~/apis/cartApi";
import otherApi from "~/apis/otherApi";
import config from "~/constants";
import { formatPrice } from "~/helpers/helpers";
import CheckoutList from "~/components/Checkout/CheckoutList";
import Address from "~/components/Checkout/Address";
import CheckoutModal from "~/components/Checkout/CheckoutModal";
import Payment from "~/components/Checkout/Payment";
import Coupon from "~/components/Checkout/Coupon";

function Checkout() {
  const [carts, setCarts] = useState([]);
  const [usd, setUsd] = useState(0.0000430156);
  const [payment, setPayment] = useState(0);
  const [openModal, toggleModal] = useToggle(false);
  const [totalPrice, setTotalPrice] = useState(0);
  const [coupon, setCoupon] = useState({ code: "NO", money: 0 });
  const [couponCode, setCouponCode] = useState("");
  const [feeship, setFeeship] = useState();
  const [address, setAddress] = useState();

  useTitle("Thanh toán");

  useEffect(() => {
    cartApi
      .getCartChecked()
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          const { carts, total_price: totalPrice } = res.data;
          setCarts(carts);
          setTotalPrice(totalPrice);
        }
      })
      .catch((err) => {});
  }, []);

  // useEffect(() => {
  //   otherApi
  //     .convertMoney()
  //     .then((res) => {
  //       const converted = res.data;
  //       const { results } = converted;
  //       setUsd(results["VND_USD"].val.toFixed(8));
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }, []);

  const showModal = () => {
    toggleModal();
  };

  return (
    <div className="checkout-section">
      <h3 className="title-section">Thanh toán</h3>
      <div className="row">
        <div className="col-12 col-md-8 col-lg-8 col-xl-8">
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <CheckoutList carts={carts} />
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <Address setFeeship={setFeeship} setAddress={setAddress} />
              <CheckoutModal
                method={payment}
                coupon={coupon}
                feeship={feeship}
                address={address}
                openModal={openModal}
                toggleModal={toggleModal}
              />
            </div>
          </div>
        </div>
        <div className="col-12 col-md-4 col-lg-4 col-xl-4">
          <div className="payment-wrapper">
            <h3 className="title-payment">Thanh toán</h3>
            <Payment payment={payment} setPayment={setPayment} />
            <Coupon
              setCoupon={setCoupon}
              couponCode={couponCode}
              setCouponCode={setCouponCode}
            />
            <div className="apply-section">
              <p className="apply-text">Địa chỉ: {address}</p>
            </div>

            {coupon.code !== "NO" && (
              <div className="apply-section">
                {coupon.code !== "NO" && (
                  <p className="apply-text">
                    Mã giảm giá: {coupon.code} - Giảm{" "}
                    {formatPrice(coupon.money)}
                  </p>
                )}
              </div>
            )}

            <div className="money-section">
              <h4 className="money-title">Tổng cộng</h4>
              <ul className="money-list">
                <li className="money-item">
                  <p className="money-desc">Tạm tính: </p>
                  <p className="money-content">
                    {payment !== 3
                      ? formatPrice(totalPrice)
                      : (totalPrice * usd).toFixed() + "$ (USD)"}
                  </p>
                </li>

                {coupon.code !== "NO" && (
                  <li className="money-item">
                    <p className="money-desc">Giảm giá: </p>
                    <p className="money-content">
                      -
                      {payment !== 3
                        ? formatPrice(coupon.money)
                        : (coupon.money * usd).toFixed() + "$ (USD)"}
                    </p>
                  </li>
                )}

                {feeship !== 0 && (
                  <li className="money-item">
                    <p className="money-desc">Phí vận chuyển: </p>
                    <p className="money-content">
                      {payment !== 3
                        ? formatPrice(feeship)
                        : (feeship * usd).toFixed() + "$ (USD)"}
                    </p>
                  </li>
                )}
                <li className="money-item">
                  <p className="money-desc">Tổng tiền: </p>
                  <p className="money-content">
                    {payment !== 3
                      ? formatPrice(totalPrice + feeship - coupon.money)
                      : (
                          (totalPrice + feeship - coupon.money) *
                          usd
                        ).toFixed() + "$ (USD)"}
                  </p>
                </li>
              </ul>
            </div>
            <div className="order-button">
              <div className="btn btn-order" onClick={showModal}>
                Đặt hàng
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Checkout;
